var url = ""
var isShow = true

function setIsShow(input) {
    isShow = input
    checkShow()
}

function checkShow() {
    if (isShow) {
        document.getElementById("glassVitural").style.opacity = `1`;
        document.getElementById("brand").style.opacity = `1`;
        document.getElementById("price").style.opacity = `1`
        document.getElementById("description").style.opacity = `1`

    } else {
        document.getElementById("glassVitural").style.opacity = `0`;
        document.getElementById("brand").style.opacity = `0`;
        document.getElementById("price").style.opacity = `0`
        document.getElementById("description").style.opacity = `0`

    }
}

function changeGlass(id) {

    const data = dataGlasses.find(item => {
        return item.id.includes(id)
    })
    url = data.virtualImg
    document.getElementById("glassVitural").style.opacity = `1`;
    document.getElementById("brand").style.opacity = `1`;
    document.getElementById("price").style.opacity = `1`
    document.getElementById("description").style.opacity = `1`
    document.getElementById("glassVitural").src = `${url}`;
    document.getElementById("brand").innerHTML = `${data.name} - ${data.brand} (${data.color})`;
    document.getElementById("price").innerHTML = `${data.price}`
    document.getElementById("description").innerHTML = `${data.description}`
};



let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];


const demoGlasses = () => {
    let glaseesValue = ""
    for (let i = 0; i < dataGlasses.length; i++) {
        let glassesItem = dataGlasses[i]
        glaseesValue += `<div> 
                    <img src= " ${glassesItem.src}" class="ml-3" width="130px" height= "135px"  onclick = "changeGlass('${glassesItem.id}')" />                
                    </div>`
    }
    document.getElementById("vglassesList").innerHTML = glaseesValue
}


demoGlasses()


